.. index::
   single: FAQ
   single: aide
   single: questions
.. _faq:

===================
foire aux questions
===================

.. toctree::
   :caption: Un peu d'aide ?
   :maxdepth: 1

   Comment utiliser cette documentation ? <design/this>
   Comment contribuer ? <contributing>
